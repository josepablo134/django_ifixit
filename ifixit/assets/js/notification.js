//Notificacion
function show_notification(title_d,text_d,image_d,sticky_d,time_d,class_name_d){
  //Se ejecutara cuando este lista la pagina
  $(document).ready(function () {
    var unique_id = $.gritter.add(
      {//DICTIONARY START
        // (string | mandatory) the heading of the notification
        title: title_d,//'Welcome to Dashgum!',
        // (string | mandatory) the text inside the notification
        text: text_d,//'Hover me to enable the Close Button. You can hide the left sidebar clicking on the button next to the logo. Free version for <a href="http://blacktie.co" target="_blank" style="color:#ffd777">BlackTie.co</a>.',
        // (string | optional) the image to display on the left
        image: image_d,//'assets/img/ui-sam.jpg',
        // (bool | optional) if you want it to fade out on its own or just sit there
        sticky: sticky_d,//true,
        // (int | optional) the time you want it to be alive for before fading out
        time: time_d,//'',
        // (string | optional) the class name you want to apply to that specific message
        class_name: class_name_d//'my-sticky-class'
      }//DICTIONARY END
    );//gritter.add END
    return false;
    }//function END
  );//ready END
}
