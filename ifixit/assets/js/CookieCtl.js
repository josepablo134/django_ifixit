/**
* @function setCookie
* Permite almacenar de forma estructurada un dato en una cookie
* @param Label STR etiqueta del dato que se desea almacenar
* @param data STR dato que se desea almacenar
* @return void
* */

function setCookie(Label,data){
    document.cookie = Label+"="+data+";"
}

function setCookieDeathTime(){
    document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
}
/**
*   @function getCookie
* Permite obtener un dato almacenado en la cookie
*   @param Label   STR etiqueta del dato que se ha almacenado
*   @return  STR dato o str vacio de no coincidir la etiqueta con ningun dato
* */
function getCookie(Label){
    var decodedCookie = decodeURIComponent(document.cookie);
    var data = decodedCookie.split(";");//Separamos los datos con formato
    for(var i=0;i< data.length; i++){//Para cada elemento con formato
      //Obtenemos el dato con formato segun indice
      var frame = data[i].split("=");//Separamos el Label del dato
      if(frame[0].substring(1)==Label){//Si el Label coincide
        return frame[1];//Retornamos el dato
      }
    }
    return "";//STR vacio si el dato no existe
}
