_toggle_ = false
function hide(){
  document.getElementById('Formulario').setAttribute("hidden","");
}
function show(){
  document.getElementById('Formulario').removeAttribute("hidden");
}
function toggle(){
	form = document.getElementById('Formulario')
	if(_toggle_){
		form.setAttribute("hidden","");
		_toggle_ = false;
	}else{
		form.removeAttribute("hidden");
		_toggle_ = true;
	}
}
function setData(){
	total_in   = document.getElementById('id_Total');
	method_in  = document.getElementById('id_MetodoPago');
	concept_in =document.getElementById('id_Concepto');
	date_in    = document.getElementById('id_Fecha');
   
  total_num  	   = document.getElementById('totalRecibo');
  total_char 	   = document.getElementById('importeEnLetras');
  concept_out	   = document.getElementById('ConceptoServicio');
  date0		   = document.getElementById('Fecha0');
  date1		   = document.getElementById('Fecha1');
  method	   = document.getElementById('Method');

	date1.innerHTML = date0.innerHTML = date_in.value;

	if( concept_in.value != "" ){
		concept_out.innerHTML = concept_in.value;
	}

	if( method_in.value != "" ){
		method.innerHTML = method_in.value
	}

	total_num.innerHTML = total_in.value;
	total_char.innerHTML = "Implementar float to Char";
}

