from django.shortcuts import render,redirect
from django.http import HttpResponse
from global_data.session_ctl import session_ctl
from .models import Client
from .forms  import UserForm
#Se crean entradas a base de datos

def new(request):
	session = session_ctl(request.session)
	if not session.is_active():
		return redirect("/")
	form = None
	error = None
	edit = None
	if request.method == 'POST':	#Comprobar formulario si hay
		if (request.POST.get('salir') == ''):
			session.logout()#Cerrar sesion y guardar en session del sistema
			session.save_session(request.session)
			return redirect("/")
		form = UserForm(request.POST)
		if form.is_valid():
			# Comprobar formulario y guardar actividad!
			try:
				client= Client.objects.get(tel= form.cleaned_data['tel'])
				error = "El numero telefonico ya existe en la base de datos para ->"+client.nombre
			except:
				client = Client()
				client.nombre = form.cleaned_data['name']
				client.domicilio = form.cleaned_data['direction']
				client.mail   = form.cleaned_data['mail']
				client.tel    = form.cleaned_data['tel']
				client.observations = form.cleaned_data['observations']
				client.save()
				return redirect("/clients/")
		else:
			error = "Compruebe los datos del formulario"
	else:
		form = UserForm()
	context = {'form':form,'error':error,'edit':edit,'nombre':session.getName()}
	return render(request,'clients/new.html',context)

def edit(request,client_id):
	session = session_ctl(request.session)
	if not session.is_active():
		return redirect("/")
	form = None
	error = None
	edit = True
	if request.method == 'POST':	#Comprobar formulario si hay
		if (request.POST.get('salir') == ''):
			session.logout()#Cerrar sesion y guardar en session del sistema
			session.save_session(request.session)
			return redirect("/")
		form = UserForm(request.POST)
		if form.is_valid():
			# Comprobar formulario y guardar actividad!
			try:
				client = Client.objects.get(id= client_id)
				try:
					#Existe un cliente con el numero telefonico, no se puede hacer nada
					client_t = Client.objects.get(tel = form.cleaned_data['tel'])
					if(client_t.id == client.id):
						# Solo si se puede, se renuevan los datos
						client.nombre = form.cleaned_data['name']
						client.domicilio = form.cleaned_data['direction']
						client.mail   = form.cleaned_data['mail']
						client.tel    = form.cleaned_data['tel']
						client.observations = form.cleaned_data['observations']
						client.save()
						return redirect('/clients/')
					else:
						error = "El numero telefonico ya existe en la base de datos para ->"+client_t.nombre
				except:
					# Solo si se puede, se renuevan los datos
					client.nombre = form.cleaned_data['name']
					client.domicilio = form.cleaned_data['direction']
					client.mail   = form.cleaned_data['mail']
					client.tel    = form.cleaned_data['tel']
					client.observations = form.cleaned_data['observations']
					client.save()
					return redirect("/clients/")
			except:
				error = "Compruebe los datos del formulario"
		else:
			if (request.POST.get('remove') == ''):
				try:
					client = Client.objects.get(id= client_id)
					client.delete()
					return redirect('/clients/')
				except:
					error = "Compruebe los datos del formulario"
	else:
		try:
			client = Client.objects.get(id=client_id)
			form = UserForm({
				'name':client.nombre,
				'direction':client.domicilio,
				'mail':client.mail,
				'tel':client.tel,
				'observations':client.observations
				})
		except:
			return redirect('/clients/')
	context = {'form':form,'error':error,'edit':edit,'nombre':session.getName()}
	return render(request,'clients/new.html',context)

def index(request):
	session = session_ctl(request.session)
	clients = None
	nombre = None
	n_clients = 0
	if session.is_active():
		if request.method == 'POST': #Comprobar si hay formulario
			if(request.POST.get('salir') == ''):#Se solicito logout
				session.logout()#Cerrar sesion y guardar en session del sistema
				session.save_session(request.session)
				return redirect("/")
		nombre = session.getName()
		clients = Client.objects.all()
		n_clients = Client.objects.count()
	else:#No se ha iniciado session y no se debe estar aqui
		return redirect("/")
	context = {'clients':clients,'nombre':session.getName(),'n_clients':n_clients}
	return render(request,'clients/index.html',context)
