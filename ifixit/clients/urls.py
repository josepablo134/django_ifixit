from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^new/$',views.new, name='new'),
    url(r'^edit/(?P<client_id>.+)/$', views.edit, name='edit')
]
