from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator

class UserForm(forms.Form):
    name      = forms.CharField(label='Tu nombre', max_length=100)
    direction = forms.CharField(label='Direccion',max_length=250,required=False, widget=forms.Textarea(attrs={'rows': 4, 'cols': 40}))
    mail      = forms.EmailField(required=False)
    tel       = forms.CharField(label='Numero telefonico',max_length=10)
    observations = forms.CharField(label='Comentarios',max_length=300,required=False, widget=forms.Textarea(attrs={'rows': 4, 'cols': 40}))
