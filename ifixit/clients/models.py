from django.db import models

# Create your models here.
class Client(models.Model):
    nombre          = models.CharField(max_length=100)
    domicilio       = models.CharField(max_length=250)
    mail            = models.CharField(max_length=100)
    tel             = models.CharField(max_length=10)
    observations    = models.CharField(max_length=300)
	
