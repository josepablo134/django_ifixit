from django.conf.urls import url

from . import views

app_name = 'users'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^new/$',views.new_user, name='new'),
    url(r'^(?P<user_id>.+)/$', views.edit, name='edit')
]
