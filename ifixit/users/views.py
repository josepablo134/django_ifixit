from django.shortcuts import render,get_object_or_404,redirect
from django.http import HttpResponse,Http404

from django.template import loader
from activities.models import Activity
from .forms  import UserForm
from .models import User_info

from global_data.session_ctl import session_ctl
#Se crean entradas a base de datos


def index(request):
	users = None
	session = session_ctl(request.session)#Creamos un objeto de control de sesion
	new_user_link = '#'
	#Guardar banderas de sesion que no se eliminan hasta que se elimine en ejecucion
	#Esta pagina unicamente carga los datos cuando se solicita
	#en un numero par de veces
	#
	if not session.is_active():
		return redirect("/")
	if request.method == 'POST':	#Comprobar formulario si hay
		if (request.POST.get('salir') == ''):
			session.logout()#Cerrar sesion y guardar en session del sistema
			session.save_session(request.session)
			return redirect("/")
	users = User_info.objects.all()
	n_activities = Activity.objects.count()
	n_users = User_info.objects.count()
	context = {'users':users,'nombre':session.getName(),
	'n_users':n_users,'n_activities':n_activities}
	return render(request,'users/index.html',context)

def edit(request,user_id):
	session = session_ctl(request.session)#Creamos un objeto de control de sesion
	error = None
	success = None
	#Guardar banderas de sesion que no se eliminan hasta que se elimine en ejecucion
	#Esta pagina unicamente carga los datos cuando se solicita
	#en un numero par de veces
	#
	if not session.is_active():
		return redirect("/users")

	if request.method == 'POST':
		form = UserForm(request.POST)

		if form.is_valid():
			if form.cleaned_data['psw1'] == form.cleaned_data['psw2']:
				#Todo ha salido OK!
				answered = True
				success = "Se han guardado los datos correctamente"
				#user = User_info()#Creamos la instancia de base de datos
				user = User_info.objects.get(id=user_id)
				flag = False
				try:
					#Si el nombre de usuario ya existe entonces se comprueba que sea de el mismo
					user_t = User_info.objects.get(user_name= form.cleaned_data['user_name'])
					if(user_t.nombre == user.nombre):
						flag = True
					else:
						error = "El nombre de usuario ya existe para -> "+user_t.nombre
						flag = False
				except:
					flag = True
				if(flag):
					#Asignamos los valores personales
					user.nombre = form.cleaned_data['name']
					user.date   = form.cleaned_data['date']
					user.tel    = form.cleaned_data['phone']
					user.mail   = form.cleaned_data['mail']
					#Asignamos los valores de usuario
					user.user_name = form.cleaned_data['user_name']
					user.psw = form.cleaned_data['psw1']
					user.job = 5 #La jerarquia mas baja
					#Si todo esta correcto se almacena en la base de datos
					user.save()
					return redirect("/users")
			else:
				error = "Las contraseñas no coinciden"
		else:
			#Comprobar formulario si hay para solicitar logout
			if (request.POST.get('salir') == ''):
				session.logout()#Cerrar sesion y guardar en session del sistema
				session.save_session(request.session)
				return redirect("/")
			if(request.POST.get('remove') == ''):
				#Eliminar elemento!
				user = User_info.objects.get(id=user_id)
				user.delete()
				return redirect("/users")
			error = 'Verifique los datos del formulario'
	#Comprobar el nombre la existencia del usuario
	try:
		link = '/users/'+user_id+'/'#Los formularios post deben ser activados con un / al final c:
		answered = False

		user = User_info.objects.get(id=user_id)
		form = UserForm({
			'name' : user.nombre,
			'user_name' : user.user_name,
			'psw1' : user.psw,
			'psw2' : user.psw,
			'mail' : user.mail,
			'date' : user.date,
			'phone': user.tel
		})
		context = {'nombre':session.getName(),'form':form,'link':link,'success':success,'error':error,'edit':True,'error':error}
		return render(request,'users/new_user.html',context)
	except:
		return redirect('/users')

def new_user(request):
	link = '/users/new/'#Los formularios post deben ser activados con un / al final c:
	name = None
	error = None
	success = None
	session = session_ctl(request.session)#Creamos un objeto de control de sesion
	#Guardar banderas de sesion que no se eliminan hasta que se elimine en ejecucion
	#Esta pagina unicamente carga los datos cuando se solicita
	#en un numero par de veces
	#
	if not session.is_active():
		return redirect("/")
#	if(session.getId is not 1):
#		return redirect("/users/")
	if request.method == 'POST':
		form = UserForm(request.POST)
		if form.is_valid():
			try:
				#Evitar que se cree otro usuario con el mismo nombre de usuario
				user_t = User_info.objects.get(user_name= form.cleaned_data['user_name'])
			except:
				if form.cleaned_data['psw1'] == form.cleaned_data['psw2']:
					#Todo ha salido OK!
					answered = True
					success = "Se han guardado los datos correctamente"
					user = User_info()#Creamos la instancia de base de datos
					#Asignamos los valores personales
					user.nombre = form.cleaned_data['name']
					user.date   = form.cleaned_data['date']
					user.tel    = form.cleaned_data['phone']
					user.mail   = form.cleaned_data['mail']
					#Asignamos los valores de usuario
					user.user_name = form.cleaned_data['user_name']
					user.psw = form.cleaned_data['psw1']
					user.job = 5 #La jerarquia mas baja
					#Si todo esta correcto se almacena en la base de datos
					user.save()
					return redirect("/users/")
				else:
					error = 'La contraseña no coincide'
			error = "El nombre de usuario ya existe para -> "+user_t.nombre		
		if request.method == 'POST':	#Comprobar formulario si hay
			if (request.POST.get('salir') == ''):
				session.logout()#Cerrar sesion y guardar en session del sistema
				session.save_session(request.session)
				return redirect("/")
	else:
		form = UserForm()
	context = {'nombre':session.getName(),'form':form,'link':link,'success':success,'error':error}
	return render(request,'users/new_user.html',context)
