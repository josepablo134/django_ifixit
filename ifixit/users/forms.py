from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
import datetime

class UserForm(forms.Form):
    name = forms.CharField(label='Tu nombre', max_length=100)
    user_name = forms.CharField(label='Nombre de usuario',max_length=30)
    psw1  = forms.CharField(label='Contraseña',widget=forms.PasswordInput)
    psw2  = forms.CharField(label='Confirma contraseña',widget=forms.PasswordInput)
    mail = forms.EmailField(required=True)
    date = forms.DateField(initial=datetime.date.today)
    phone = forms.CharField(label='Numero telefonico',max_length=10)
