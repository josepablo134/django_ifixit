from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.utils import timezone
from global_data.session_ctl import session_ctl
from .forms import date_filter,Acitivity_Form,TicketForm

from .models	import Activity
from clients.models import Client
from users.models import User_info
import datetime

from django.db 	import models

def new(request):
	session = session_ctl(request.session)
	error = None
	if not (session.is_active()):
            return redirect("/")
	nombre = session.getName()
	if request.method == 'POST':	#Comprobar formulario si hay
		if (request.POST.get('salir') == ''):#Se solicito logout
			session.logout()#Cerrar sesion y guardar en session del sistema
			session.save_session(request.session)
			return redirect("/")
		form = Acitivity_Form(request.POST)
		if(form.is_valid()):
			try:
				register = Activity()
				register.cliente 			= 	Client.objects.get( id = form.cleaned_data['cliente'])
				register.tecnico 			= 	User_info.objects.get( id =	form.cleaned_data['tecnico'])
				register.fecha_diagnostico 	= 	form.cleaned_data['diagnostic_date']
				register.comentarios		= 	form.cleaned_data['comment']
				register.diagnostico		=   form.cleaned_data['diagnostic']
				register.equipo				= 	form.cleaned_data['object']
				register.marca				=	form.cleaned_data['mark']
				register.tipo				=	form.cleaned_data['type']
				register.serie				=	form.cleaned_data['serie']
				register.estado_actual		=	form.cleaned_data['state']
				register.imei				=	form.cleaned_data['imei']
				register.id_activity		=	form.cleaned_data['imei'] + str(form.cleaned_data['diagnostic_date'])
				register.save()
				if( form.cleaned_data['print'] == True):
					return redirect("/activities/ticket/"+str(register.id))
				else:
					return redirect("/activities/")
			except:
				error = "Verifica los datos del formulario"
		else:
			error = "Verifica los datos del formulario"
	form = Acitivity_Form()
	clients_ids =	Client.objects.all().order_by('-nombre')
	users_ids   =	User_info.objects.all().order_by('-nombre')
	context = { 'form':form , 'clients':clients_ids , 'users':users_ids,
				'error':error,'nombre':nombre}
	return render(request,'activities/new.html',context)

def ticket(request,activity_id):
	session = session_ctl(request.session)
	if not (session.is_active()):
            return redirect("/")
	activity = None
	try:
		activity = Activity.objects.get(id = activity_id)
	except:
		return redirect("/activities/")

	form = TicketForm({	"Fecha": datetime.date.today(),
				"Concepto":"Servicio de reparacion",
				"MetodoPago":"Efectivo",
				"Total":0.0
				})

	Empresa 	= 	"Refacciones y accesorios DCG"
	Empresa_Dir	=	"C.28 # 206 x 25A y 27. Ticul, Yucatán"
	Empresa_Tel	=	["Tel. 972-1035","Cel. 997-103-4547","Cel. 997-103-4537"]
	ticket_nro  = activity.id   #	"0001 - 00000168"
	ticket_date = activity.fecha_diagnostico.date()
	

	context = {'ticket':False,'Empresa_Dir':Empresa_Dir,'Empresa':Empresa,'Empresa_Tel':Empresa_Tel,
	'ticket_nro':ticket_nro,'ticket_date':ticket_date,'activity':activity,'form':form}

	return render(request,'activities/ticket.html',context)
def edit(request,activity_id):
	session = session_ctl(request.session)
	state_list = ['Revisado','En Reparacion','Listo','Entregado']
	activity_state = None
	error = None
	if not (session.is_active()):
            return redirect("/")
	nombre = session.getName()
	if request.method == 'POST':	#Comprobar formulario si hay
		if (request.POST.get('salir') == ''):#Se solicito logout
			session.logout()#Cerrar sesion y guardar en session del sistema
			session.save_session(request.session)
			return redirect("/")
		if (request.POST.get('print') == ''):#Se solicito logout
			#Guardar algo en la session
			return redirect("/activities/ticket/"+activity_id)
		form = Acitivity_Form(request.POST)
		if(form.is_valid()):
			try:
				register = Activity.objects.get(id = activity_id)
				register.estado_actual		=	form.cleaned_data['state']
				register.comentarios		=   form.cleaned_data['comment']
				register.diagnostico	    =   form.cleaned_data['diagnostic']
				register.save()
				return redirect("/activities/")
			except:
				error = "Verifica los datos del formulario"
		else:
			error = "Verifica los datos del formulario"
	try:
		register = Activity.objects.get(id = activity_id)
		#Mostrar los datos en pantalla
		form = Acitivity_Form({
			'cliente':	register.cliente.id,
			'tecnico':	register.tecnico.id,
			'diagnostic_date': register.fecha_diagnostico,
			'comment':	register.comentarios,
			'diagnostic':	register.diagnostico,
			'object':	register.equipo,
			'mark'	:	register.marca,
			'type'	:	register.tipo,
			'serie'	:	register.serie,
			'imei'	:	register.imei,
			'state'	:	register.estado_actual
			})
		activity_state = state_list[ register.estado_actual ]
	except:
		return redirect("/activities/")
	clients_ids =	Client.objects.all().order_by('-nombre')
	users_ids   =	User_info.objects.all().order_by('-nombre')
	context = { 'form':form , 'clients':clients_ids , 'users':users_ids,
				'error':error,'activity_state':activity_state,'nombre':nombre}
	return render(request,'activities/edit.html',context)
	#HttpResponse('Aqui se editan las actividades <a href=\"/activities/ticket/'+ activity_id +'\"> Ticket </a> ')

def index(request):
	session = session_ctl(request.session)
	activity_list   = {'0':'Revisado','1':'En Reparacion','2':'Listo','3':'Entregado'}
	nombre = None
	n_activities = 0
	activities = None
	form = None
	context = None
	msj_0 = "Actividades"
	if not (session.is_active()):
            return redirect("/")
	if request.method == 'POST':	#Comprobar formulario si hay
		if (request.POST.get('salir') == ''):#Se solicito logout
			session.logout()#Cerrar sesion y guardar en session del sistema
			session.save_session(request.session)
			return redirect("/")
		form = date_filter(request.POST)
		if(form.is_valid()):
			#RECUPERAR DATOS DEL FORMULARIO
			dat = form.cleaned_data['date']
			activities 	 = Activity.objects.filter(fecha_diagnostico__gte=
				datetime.date(dat.year,dat.month,dat.day))[:100]
			n_activities = len(activities)
			msj_0 += " para %d/%d/%d"%(dat.day,dat.month,dat.year)
		else:
			n_activities = Activity.objects.count()
			activities = Activity.objects.all()[:100]
	else:
		form = date_filter()
		n_activities = Activity.objects.count()
		activities = Activity.objects.all().order_by('estado_actual')[:100]
	nombre = session.getName()
	context = {'nombre':nombre,'activities':activities,
		'form':form,'n_activities':n_activities,'msj_0':msj_0,'activity_list':activity_list}
	return render(request,'activities/index.html',context)
