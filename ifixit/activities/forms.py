from django import forms
import datetime
class date_filter(forms.Form):
    date = forms.DateField(initial=datetime.date.today,
                           widget=forms.TextInput(attrs=
                                {'id':'datepicker'})
                           )
class Acitivity_Form(forms.Form):
    #Almacenar el id del cliente
    cliente         =   forms.IntegerField(label='Cliente')
    #Almacenar el id del tecnico
    tecnico         =   forms.IntegerField(label='Tecnico')
    diagnostic_date =   forms.DateField(label='fecha de diagnostico',initial=datetime.date.today)
    comment         =   forms.CharField(label='Observaciones', max_length=300, widget=forms.Textarea(attrs={'rows': 4, 'cols': 40}))
    diagnostic      =   forms.CharField(label='diagnostico', max_length=300,widget=forms.Textarea(attrs={'rows': 4, 'cols': 40}))
    object          =   forms.CharField(label='Equipo', max_length=100)
    mark            =   forms.CharField(label='Marca', max_length=100)
    type            =   forms.CharField(label='Tipo de equipo', max_length=100)
    serie           =   forms.CharField(label='Numero de serie', max_length=100)
    state           =   forms.IntegerField(label='Estado de la actividad')
    imei            =   forms.CharField(label='IMEI', max_length=20)
    print	    =   forms.BooleanField(required=False,initial=True)

class TicketForm(forms.Form):
    Concepto    = forms.CharField(label='Concepto de pago')
    MetodoPago  = forms.CharField(label='Metodo de pago')
    Total       = forms.FloatField(label='Total')
    Fecha       = forms.DateField(label='Fecha de emición',initial=datetime.date.today)
    #ReciboNumero= forms.CharField(label='Numero de recibo')
