from django.db 	import models
from django.utils import timezone
from users 	 import models as user_models
from clients import models as client_models

class Activity(models.Model):
#id_activity		=       models.AutoField(primary_key=True)
#Enlazar con un unico cliente
    cliente = models.ForeignKey(client_models.Client, on_delete=models.CASCADE)
#Enlazar con un unico tecnico
    tecnico = models.ForeignKey(user_models.User_info, on_delete=models.CASCADE)
    fecha_diagnostico = models.DateTimeField('fecha de diagnostico')
    comentarios = models.CharField(max_length=300)
    diagnostico = models.CharField(max_length=300)
    equipo = models.CharField(max_length=60)
    marca = models.CharField(max_length=60)
    tipo = models.CharField(max_length=20)
    serie = models.CharField(max_length=30)
    estado_actual = models.IntegerField(default=0)
    imei = models.CharField(max_length=16,default='IMEI')
