from users.models      import User_info as user_model
from clients.models    import Client	as client_model
from activities.models import Activity  as activity_model



class session_ctl:
	__id = None
	__nombre = None
	def __init__(self,session_dict=None):
		if(session_dict):
			self.set_session(session_dict)
	def set_session(self,session_dict):
		try:
			self.__nombre = session_dict['nombre']
			self.__id	  = session_dict['id']
		except:
			return None#Error, session no existe, simplemente no se cargan datos
	def logout(self):
		self.__clear()
	def login(self,id,password):
		#Log In de usuarios
		try:
			match_user = user_model.objects.get(user_name=id)
			if match_user.psw == password:
				self.__nombre = match_user.nombre
				self.__id 	  = match_user.job
				return True
			else:
				self.__clear()
				return False
		except:#Los datos no existen en la base de datos
			return False
	def is_active(self):
		if self.__nombre == None:
			return False
		return True
	def getName(self):
		return self.__nombre
	def setName(self,name):
		self.__nombre = name
	def getId(self):
		return self.__id
	def setId(self,id):
		self.__id = id
	def save_session(self,session_dict):
		session_dict['nombre'] = None
		session_dict['id'] 	   = None
	def __str__(self):
		return ''
	def __repr(self):
		return ''
	def __clear(self):
		self.__id = None
		self.__nombre = None
