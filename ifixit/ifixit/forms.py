from django import forms

class LogInForm(forms.Form):
    user_name   = forms.CharField(label='Nombre de usuario',max_length=30,required=True)
    psw         = forms.CharField(label='Contraseña',widget=forms.PasswordInput,required=True)
    remember    = forms.BooleanField(required=False)
