from django.shortcuts import render,get_object_or_404
from django.http import HttpResponse,Http404
from .forms  import LogInForm
from users.models import User_info
from activities.models import Activity
from clients.models import Client
from django.template import loader
from global_data.session_ctl import session_ctl

def index(request):
	activities = []
	activity_list   = {'0':'Revisado','1':'En Reparacion','2':'Listo','3':'Entregado'}	
	n_activities = 0
	n_clients = 0
	error = None
	action_link = '/'
	nombre = ''
	userid = 5
	session = session_ctl(request.session)#Creamos un objeto de control de sesion
	if request.method == 'POST':	#Comprobar formulario si hay
		login = LogInForm(request.POST)
		if login.is_valid():
			if session.login(login.cleaned_data['user_name'],login.cleaned_data['psw']):
				#Sesion activa
				request.session['nombre'] = session.getName()
				request.session['id']	  = session.getId()
			else:
				#Error al iniciar sesion
				error = "El nombre de usuario o contraseña es incorrecto."
		else:
			if (request.POST.get('salir') == ''):
				session.logout()#Cerrar sesion y guardar en session del sistema
				session.save_session(request.session)
	if session.is_active():
		n_activities = Activity.objects.count()
		activities = Activity.objects.all().order_by('estado_actual')[:20]
		n_clients = Client.objects.count()
		clients   = Client.objects.all()[:20]
		nombre = session.getName()
		userid = session.getId()
		context = {'nombre':nombre,'error':error,'userid':userid,
		'n_activities':n_activities,'n_clients':n_clients,'activities':activities,'clients':clients,
		'activity_list':activity_list}
		return render(request,'ifixit/index.html',context)
	else:
		#Mostrar login
		login = LogInForm()
		context = {'error':error,'login':login,'action_link':action_link}
		return render(request, 'ifixit/index.html',context)
